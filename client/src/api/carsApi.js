import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "http://localhost:3002",
  headers: {
    "Content-type": "application/json",
  },
});

//get cars
export const getCarsList = async () => {
  const result = await axiosInstance
    .get("/cars")
    .catch((error) => console.log(error));
  return result;
};

//delete a resource
export const deleteCarItem = async (carId) => {
  try {
    console.log({ carId });
    await axiosInstance.delete("/cars", {
      data: { carId },
    });
    return true;
  } catch (err) {
    console.log(err);
    return false;
  }
};
