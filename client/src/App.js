import React from "react";
import logo from "./logo.svg";
import "./App.css";
import ListCarsContainer from "./components/ListCarsContainer";

function App() {
  return <ListCarsContainer></ListCarsContainer>;
}

export default App;
