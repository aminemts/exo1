import React, { Component } from "react";
import { getCarsList, deleteCarItem } from "../api/carsApi";
import ListCars from "./ListCars";

class ListCarsContainer extends Component {
  state = {
    carsData: [],
  };

  getData = async () => {
    const result = await getCarsList();
    console.log({ result });
    this.setState({ carsData: result.data });
  };

  deleteCarById = async (carId) => {
    const result = await deleteCarItem(carId);
    console.log({ carId, result });
    if (result)
      this.setState({
        carsData: this.state.carsData.filter((item) => item.carId !== carId),
      });
  };

  componentDidMount() {
    this.getData();
  }

  render() {
    return (
      <div>
        <h1>Liste des voitures</h1>
        <ListCars
          carsData={this.state.carsData}
          deleteCarById={this.deleteCarById}
        ></ListCars>
      </div>
    );
  }
}

export default ListCarsContainer;
