import React from "react";
import { Container, Row, Col } from "reactstrap";

import "./carItem.css";

const CarItem = ({ model, reference, color, handleDelete }) => {
  return (
    <div>
      <Col sm="4">
        <div className="carItem">
          <p>model: {model}</p>
          <p>reference: {reference}</p>
          <p>color: {color}</p>
          <button className="btn-danger" onClick={handleDelete}>
            Delete
          </button>
        </div>
      </Col>
    </div>
  );
};

export default CarItem;
