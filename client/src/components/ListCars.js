import React from "react";
import CarItem from "./CarItem";

const ListCars = ({ carsData, deleteCarById }) => {
  return carsData.map((item) => {
    const carProps = {
      ...item,
      handleDelete: () => deleteCarById(item.carId),
    };
    return <CarItem key={item.carId} {...carProps}></CarItem>;
  });
};

export default ListCars;
