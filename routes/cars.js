const carRoutes = (app, fs) => {
  //variables
  const data = [
    {
      carId: "1",
      model: "mercedes",
      reference: "class c",
      color: "blue",
    },
    {
      carId: "2",
      model: "bmw",
      reference: "serie 1",
      color: "black",
    },
    {
      carId: "3",
      model: "audi",
      reference: "a5",
      color: "green",
    },
    {
      carId: "4",
      model: "peugeot",
      reference: "208",
      color: "white",
    },
    {
      carId: "5",
      model: "renaut",
      reference: "civic",
      color: "red",
    },
  ];

  //READ
  app.get("/cars", (req, res) => {
    res.send(data);
  });

  app.delete("/cars", (req, res) => {
    console.log(req.body);
    res.send("Car is deleted");
  });
};

module.exports = carRoutes;
