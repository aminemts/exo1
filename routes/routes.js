const carRoutes = require("./cars");

const appRouter = (app, fs) => {
  app.get("/", (req, res) => {
    res.send("welcome");
  });

  //run our car route module here
  carRoutes(app, fs);
};

module.exports = appRouter;
